module.exports = function (eleventyConfig) {
  eleventyConfig.addCollection('postRecent', collection => {
    // Note: work around limitation in the liquid template language
    return collection.getFilteredByTag('post').reverse();
  });
  eleventyConfig.addPassthroughCopy('src/media');
  eleventyConfig.addPassthroughCopy('src/style');
  return {
    templateFormats: [
      'gif', // Note: uses passthroughFileCopy
      'html',
      'md'
    ],
    dir: {
      input: 'src',
      output: 'dist'
    },
    passthroughFileCopy: true
  };
};
