---
layout: page-en
permalink: "/en/about/"
---

# Lars Thorup
	
![Lars](../../media/DSC1108-small.jpg)

Lars Thorup is an experienced software developer and architect with a flair for mentoring. He is a frequent speaker at conferences in Europe and the US. Lars’s technical expertise covers embedded software in C++, server software in Node.js and C# and web development in JavaScript. Lars values clean code, sound architecture, thorough test coverage and frequent releases.

Lars can help you and your team improve your software quality and become more efficient. Get in touch to discuss your needs.
